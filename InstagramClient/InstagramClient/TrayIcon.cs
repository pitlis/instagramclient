﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace InstagramClient
{
    class TrayIcon
    {
        NotifyIcon m_notifyIcon;
        MainWindow myWindow;
        
        public TrayIcon(MainWindow myWin)
        {
            myWindow = myWin;
            CreateNotifyIcon();
        }

        private void CreateNotifyIcon()
        {
            m_notifyIcon = new System.Windows.Forms.NotifyIcon();
            m_notifyIcon.BalloonTipTitle = "Зоголовок сообщения";
            m_notifyIcon.BalloonTipText = "Появляется когда мы помещаем иконку в трэй";
            m_notifyIcon.Text = "Это у нас пишется если мы наведем мышку на нашу иконку в трэее";
            m_notifyIcon.Icon = new System.Drawing.Icon(@"c:\hm.ico");
            m_notifyIcon.Click += new EventHandler(m_notifyIcon_Click);
        }

        public WindowState m_storedWindowState = WindowState.Normal;
        public void OnStateChanged()
        {
            if (myWindow.WindowState == WindowState.Minimized)
            {
                myWindow.Hide();
                //отображение подсказки
                if (m_notifyIcon != null)
                {
                    m_notifyIcon.ShowBalloonTip(2000);
                }
            }
            else
                m_storedWindowState = myWindow.WindowState;
        }

        void m_notifyIcon_Click(object sender, EventArgs e)
        {
            myWindow.Show();
            myWindow.WindowState = m_storedWindowState;
        }

        void ShowTrayIcon(bool show)
        {
            if (m_notifyIcon != null)
                m_notifyIcon.Visible = show;
        }

        public void CheckTrayIcon()
        {
            ShowTrayIcon(!myWindow.IsVisible);
        }

        public void Destroy()
        {
            m_notifyIcon.Dispose();
            m_notifyIcon = null;
        }
        
    }
}

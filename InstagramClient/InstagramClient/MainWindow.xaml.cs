﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstagramClient
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TrayIcon trayIcon;//отвечает за сворачивание-разворачивание окна и иконку в трее

        public MainWindow()
        {
            InitializeComponent();
            trayIcon = new TrayIcon(myWindow);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }





        void OnStateChanged(object sender, EventArgs args)
        {
            trayIcon.OnStateChanged();
        }
        void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            trayIcon.CheckTrayIcon();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            trayIcon.Destroy();
        }
    }
}

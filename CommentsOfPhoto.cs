﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaseInst
{
    [Serializable]
    class CommentsOfPhoto
    {
        List<string> Authors;
        List<DateTime> Dates;
        List<string> Comments;

        public struct CommentType
        {
            public string Author;
            public DateTime Date;
            public string Comment;
        }

        int CountComments;

        public CommentsOfPhoto()
        {
            Authors = new List<string>();
            Dates = new List<DateTime>();
            Comments = new List<string>();
            CountComments = 0;
        }

        public void SetComment(string A, DateTime D, string C)
        {
            Authors.Add(A);
            Dates.Add(D);
            Comments.Add(C);
            CountComments++;
        }
        public int GetCountComments()
        {
            return CountComments;
        }
        public CommentType GetComment(int Index)
        {
            CommentType comment;
            comment.Author = String.Copy(Authors[Index]);
            comment.Date = Dates[Index];
            comment.Comment = String.Copy(Comments[Index]);
            return comment;
        }
    }
}

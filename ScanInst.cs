﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Drawing;
using System.Text.RegularExpressions;

namespace inst
{
    class ScanInst
    {        
        string webpage;
        string ProfileUrl;
        BaseInst.BaseInst Base;
        public ScanInst(string link, BaseInst.BaseInst myBase)
        {
            ProfileUrl = link;
            Base = myBase;
        }

        public void Scan()
        {
            webpage = DownloadPage();
            Processing(webpage);
        }
        //public void FullScan();

        private void Processing(string webpage)
        {
            SearchNewPhotos(webpage);
            CheckCommentsAndLikes(webpage);
        }

        //Ищет новые фото и добавляет в базу
        private void SearchNewPhotos(string webpage)
        {
            if (GetDate(webpage, true) > Base.GetRecord(Base.GetCount()).date)//если появилась новая фотография
            {
                //сравнение всех фоток со страницы с содержимым базы - только факт наличия фотки
                CheckPhotos(webpage);
            }
        }

        //Ищет только новые фото (сравнивает с содержимым базы). Если найдены - добавляет в базу
        private void CheckPhotos(string webpage)
        {
            string pLink = "\"link\":\"http[^&]*?\"";
            string pDate = "\"created_time\":\"[0-9]+\",\"images\":";

            Match matchLink = Regex.Match(webpage, pLink);
            Match matchDate = Regex.Match(webpage, pDate);

            while (matchLink.Success && matchDate.Success)
            {
                DateTime date = GetDate(matchDate.Value, true);
                string link = GetLink(matchLink);
                if (!FoundPhotoInBase(link, date))
                    AddNewPhoto(matchLink, webpage, date);

                matchLink = matchLink.NextMatch();
                matchDate = matchDate.NextMatch();
            }  
        }

        //Сравнивает количество комментариев и лайков с базой. Если найдены - добавляет в базу
        private void CheckCommentsAndLikes(string webpage)
        {
            string pLink = "\"link\":\"http[^&]*?\"";
            string pLikesCount = "\"likes\":[^&]?\"count\":[0-9]+";
            string pCommentsCount = "\"comments\":[^&]?\"count\":[0-9]+";

            Match matchLink = Regex.Match(webpage, pLink);
            Match matchLikeCount = Regex.Match(webpage, pLikesCount);
            Match matchCommentCount = Regex.Match(webpage, pCommentsCount);

            while (matchLink.Success && matchLikeCount.Success && matchCommentCount.Success)
            {
                string link = GetLink(matchLink);
                int CountLike = GetNumber(matchLikeCount);
                int CountComment = GetNumber(matchCommentCount);
                BaseInst.BaseInst.RecordPhoto Record = GetRecordFromBD(link);

                if (CountLike > Record.likesCount)
                    UpdateLikeCount(link, CountLike);
                if (CountComment > Record.commentsCount)
                {
                    UpdateComments(link);
                }

                matchLink = matchLink.NextMatch();
                matchLikeCount = matchLikeCount.NextMatch();
                matchCommentCount = matchCommentCount.NextMatch();
            }
        }

        private string DownloadPage()
        {
            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            return client.DownloadString(ProfileUrl);
        }
        private Image DownloadPhoto(string link)
        {
            WebClient wc = new WebClient();
            Image img = Image.FromStream(wc.OpenRead(new Uri(link)));
            //wc.DownloadFileAsync(new Uri(link), "D:\\games\\test\\" + System.IO.Path.GetFileName(link));
            return img;
        }

        private DateTime GetDate(string text, bool image)
        {
            string pDate;
            if (image)
                pDate = "\"created_time\":\"[0-9]+\",\"images\":";
            else pDate = "\"created_time\":\"[0-9]+\"";

            Match match = Regex.Match(text, pDate);
            int dateNumber = GetNumber(match);
            DateTime date = new DateTime(1970, 1, 1).AddSeconds(dateNumber);

            return date;
        }
        private string GetLink(Match match)
        {
            string link = match.Value;
            link = Regex.Replace(link, "\\\\", "");
            link = Regex.Replace(link, "\"link\":", "");
            link = Regex.Replace(link, "\"url\":", "");
            link = Regex.Replace(link, "\"standard_resolution\":", "");
            link = Regex.Replace(link, "\"", "");
            link = Regex.Replace(link, "{", "");
            return link;
        }
        private int GetNumber(Match match)
        {
            string s = match.Value;
            s = Regex.Replace(s, "[^0-9]", "");

            return Int32.Parse(s);
        }

        private bool FoundPhotoInBase(string link, DateTime date)
        {
            return false;
        }
        private void AddNewPhoto(Match mLink, string webpage, DateTime date)
        {
            string pLink = "\"link\":\"http[^&]*?\"";
            string pLinkStandartImage = "\"standard_resolution\":[^&]?\"url\":\"http[^&]*?\"";

            Match matchLink = Regex.Match(webpage, pLink);
            Match matchPhoto = Regex.Match(webpage, pLinkStandartImage);

            while (!mLink.Value.Equals(matchLink.Value))
            {
                matchLink = matchLink.NextMatch();
                matchPhoto = matchPhoto.NextMatch();
            }
            string PhotoLink = GetLink(matchPhoto);
            Image img = DownloadPhoto(PhotoLink);
            Base.Create(matchLink.Value, img, date);
        }
        private BaseInst.BaseInst.RecordPhoto GetRecordFromBD(string link)
        {
            int Index = Base.GetIndex(link);
            return Base.GetRecord(Index);
        }
        private void UpdateLikeCount(string link, int count)
        {
            int Index = Base.GetIndex(link);
            if (Index >= 0)
                Base.UpdateLikesCount(Index, count);
        }
        //сравнивает комментарии в базе со всеми комментариями к фото, добавляет новые
        private void UpdateComments(string link)
        {
            int Index = Base.GetIndex(link);
            if (Index >= 0)
            {
                GetAllCommentsFromPhoro(link);
            }




        }

        private List<BaseInst.CommentsOfPhoto.CommentType> GetAllCommentsFromPhoro(string link)
        {
            List<BaseInst.CommentsOfPhoto.CommentType> Comments = new List<BaseInst.CommentsOfPhoto.CommentType>();
            string pLink = "\"link\":\"http[^&]*?\"";
            string pComments = "\"comments\":[^&]?\"count\":[0-9]+,\"data\":[^&]+]";
            string pComment = "\"created_time\":[^&]+\"id\":\"[0-9]+\"";

            Match matchLink = Regex.Match(webpage, pLink);
            Match matchComments = Regex.Match(webpage, pComments);

            while (link.Equals(matchLink.Value))
            {
                matchLink = matchLink.NextMatch();
                matchComments = matchComments.NextMatch();
            }

            string pAuthor = "\"from\":[^&]?\"username\":\"";
            string pCommentText = "\"text\":\"[^&]+\",\"from\"";
            Match matchComment = Regex.Match(matchComments.Value, pComment);
            while (matchComment.Success)
            {
                BaseInst.CommentsOfPhoto.CommentType comment;
                comment.Date = GetDate(matchComment.Value, false);
                comment.Author = Regex.Match(matchComment.Value, pAuthor).Value;
                string text = Regex.Match(matchComment.Value, pCommentText).Value;
                text = Regex.Replace(text, "\"text\":\"", "");
                text = Regex.Replace(text, "\",\"from\"", "");
                comment.Comment = System.Net.WebUtility.HtmlDecode(text);

                Comments.Add(comment);
                matchComment = matchComment.NextMatch();
            }
            return Comments;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BaseInst
{
    [Serializable]
    class BaseInst
    {
        public struct RecordPhoto
        {
            public string link;
            public int commentsCount;
            public int likesCount;
            public int photoIndex;
            public int commentsIndex;
            public DateTime date;
        }

        List<RecordPhoto> Base;
        List<CommentsOfPhoto> BaseComments;
        List<Image> BaseImages;

        public BaseInst()
        {
            Base = new List<RecordPhoto>();
            BaseComments = new List<CommentsOfPhoto>();
            BaseImages = new List<Image>();
        }

        public int GetCount()
        {
            return Base.Count();
        }

        //CRUD
        public void Create(string l, Image img, DateTime D)
        {
            RecordPhoto newRecord;
            newRecord.link = String.Copy(l);
            newRecord.commentsCount = 0;
            newRecord.likesCount = 0;
            newRecord.date = D;

            BaseComments.Add(new CommentsOfPhoto());
            BaseImages.Add(img);

            newRecord.photoIndex = BaseImages.Count();
            newRecord.commentsIndex = BaseImages.Count();

            Base.Add(newRecord);
        }


        public int GetIndex(string link)
        {
            for (int i = 0; i < Base.Count(); ++i)
            {
                if (Base[i].link.Equals(link))
                    return i;
            }
            return -1;
        }
        public RecordPhoto GetRecord(int Index)
        {
            if (Index < Base.Count())
                return Base[Index];
            else return new RecordPhoto();
        }

        public Image GetPhoto(int Index)
        {
            //if (Index < BaseImages.Count())
                return BaseImages[Index];
        }
        public List<CommentsOfPhoto.CommentType> GetComments(int Index)
        {
            List<CommentsOfPhoto.CommentType> comments = new List<CommentsOfPhoto.CommentType>();
            int count = BaseComments[Index].GetCountComments();
            for (int i = 0; i < count; ++i)
                comments.Add(BaseComments[Index].GetComment(i));
            return comments;
        }


        public void UpdateLikesCount(int Index, int lC)
        {
            if (Index < Base.Count())
            {
                RecordPhoto record = Base[Index];
                record.likesCount = lC;
                Base[Index] = record;
            }
        }

        public void AddComment(int Index, string A, DateTime D, string C)
        {
            BaseComments[Index].SetComment(A, D, C);
        }
    }
}
